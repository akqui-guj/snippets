#!/bin/bash

# Jenkins Server
# lokales Jenkins-Home Verzeichnis anlegen und Berechtigungen setzen
mkdir /var/lib/jenkins
chmod 777 /var/lib/jenkins

# SSH-Keys für Jenkins Slave Zugriff erzeugen
docker build -t keygen-container .

# SSH-Keys in Jenkins-Home Verzeichnis kopieren
docker run -v /var/lib/jenkins:/keys --rm keygen-container

# Public SSH-Key auf Jenkins Slave kopieren
scp /var/lib/jenkins/id_rsa.pub admin@jslave.mine.local:.

# Jenkins Server starten
docker run -p 8080:8080 -v /var/lib/jenkins:/var/jenkins_home -d --name jenkins --restart unless-stopped jenkins/jenkins:lts