#!/bin/bash
mkdir /var/libe/nexus-data
chmod 777 /var/lib/nexus-data

# Nexus Server  starten
docker run -d --name nexus -v /var/lib/nexus-data:/nexus-data -p 8081:8081 -p 5000:5000 --restart unless-stopped stefanprodan/nexus

# Firewall für Port 2222 ssh öffnen
sudo firewall-cmd --zone=public --add-port=5000/tcp --permanent
sudo firewall-cmd --zone=public --add-port=8081/tcp --permanent
sudo firewall-cmd --reload