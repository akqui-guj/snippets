#!/bin/bash

# Jenkins Slave
# lokales Jenkins-Home Verzeichnis anlegen und Berechtigungen setzen
mkdir /var/lib/jenkins
chmod 777 /var/lib/jenkins-slave

# Jenkins Slave Image bauen
docker build -t jenkins-slave .

# Jenkins Slave starten
docker run -d --name jenkins-slave -p 2222:22 -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/jenkins-slave:/home/jenkins --restart unless-stopped jenkins-slave

# Firewall für Port 2222 ssh öffnen
sudo firewall-cmd --zone=public --add-port=2222/tcp --permanent
sudo firewall-cmd --zone=public --add-port=2376/tcp --permanent
sudo firewall-cmd --reload