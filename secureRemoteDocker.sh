#!/bin/bash
WORK_DIR="$(pwd)/securedocker"
SERVER_SUBPATH="etc/docker/ssl"
CLIENT_SUBPATH=".docker"

CA_PRIV_KEY=ca-key.pem
CA_PUB_KEY=ca.pem

SERVER_PRIV_KEY=server-key.pem
SERVER_CERT_REQ=server.csr
SERVER_CERT=server-cert.pem

CLIENT_PRIV_KEY=key.pem
CLIENT_CERT_REQ=client.csr
CLIENT_CERT=cert.pem

SERVER=deploy.mine.local

if [ ! -d "$WORK_DIR" ]
then
	echo "no securedocker dir, create it..."
    mkdir $WORK_DIR
else
    echo "securedocker exists, remove and create new..."
    rm -rf $WORK_DIR
    mkdir $WORK_DIR
fi

cd $WORK_DIR

echo "create $CLIENT_SUBPATH..."
mkdir $CLIENT_SUBPATH

echo "create $SERVER_SUBPATH..."
mkdir -p $SERVER_SUBPATH

echo "generate ca private key in $CLIENT_SUBPATH/$CA_PRIV_KEY ..."
openssl genrsa -aes256 -out $CLIENT_SUBPATH/$CA_PRIV_KEY 4096

echo "generate ca public key in $CLIENT_SUBPATH/$CA_PUB_KEY..."
openssl req -x509 -new -nodes -key .docker/$CA_PRIV_KEY -days 365 -sha256 -out $CLIENT_SUBPATH/$CA_PUB_KEY -subj '/CN=docker-CA'

echo "copy ca certificate to server dir..."
cp .docker/$CA_PUB_KEY $SERVER_SUBPATH/$CA_PUB_KEY

echo "create server private key in $SERVER_SUBPATH/$SERVER_PRIV_KEY..."
openssl genrsa -out $SERVER_SUBPATH/$SERVER_PRIV_KEY 4096

echo "create server ca signing request in $SERVER_SUBPATH/$SERVER_CERT_REQ..."
openssl req -subj "/CN=$SERVER" -sha256 -new -key $SERVER_SUBPATH/$SERVER_PRIV_KEY -out $SERVER_SUBPATH/$SERVER_CERT_REQ

echo "create ssl config for generation of server certificate in $SERVER_SUBPATH/extfile.cnf..."
echo subjectAltName = DNS:$SERVER,IP:10.0.1.13,IP:127.0.0.1 > $SERVER_SUBPATH/extfile.cnf
echo extendedKeyUsage = serverAuth >> $SERVER_SUBPATH/extfile.cnf

echo "create server certificate in $SERVER_SUBPATH/$SERVER_CERT..."
openssl x509 -req -days 365 -sha256 -in $SERVER_SUBPATH/$SERVER_CERT_REQ -CA $SERVER_SUBPATH/$CA_PUB_KEY -CAkey $CLIENT_SUBPATH/$CA_PRIV_KEY \
  -CAcreateserial -out $SERVER_SUBPATH/$SERVER_CERT -extfile $SERVER_SUBPATH/extfile.cnf

echo "create client private key in $CLIENT_SUBPATH/$CLIENT_PRIV_KEY..."
openssl genrsa -out $CLIENT_SUBPATH/$CLIENT_PRIV_KEY 4096

echo "create client ca signing request in $CLIENT_SUBPATH/$CLIENT_CERT_REQ..."
openssl req -subj '/CN=client' -new -key $CLIENT_SUBPATH/$CLIENT_PRIV_KEY -out $CLIENT_SUBPATH/$CLIENT_CERT_REQ

echo "create ssl config for generation of client certificate in $CLIENT_SUBPATH/extfile.cnf..."
echo extendedKeyUsage = clientAuth >> $CLIENT_SUBPATH/extfile.cnf

echo "create client certificate in $CLIENT_SUBPATH/$CLIENT_CERT..."
openssl x509 -req -days 365 -sha256 -in $CLIENT_SUBPATH/$CLIENT_CERT_REQ -CA $CLIENT_SUBPATH/$CA_PUB_KEY -CAkey $CLIENT_SUBPATH/$CA_PRIV_KEY \
  -CAcreateserial -out $CLIENT_SUBPATH/$CLIENT_CERT -extfile $CLIENT_SUBPATH/extfile.cnf

echo "remove client and server ca signing request..."
rm -v $CLIENT_SUBPATH/$CLIENT_CERT_REQ $SERVER_SUBPATH/$SERVER_CERT_REQ

echo "make key files only readable for owner..."
chmod -v 0400 $CLIENT_SUBPATH/$CA_PRIV_KEY $CLIENT_SUBPATH/$CLIENT_PRIV_KEY $SERVER_SUBPATH/$SERVER_PRIV_KEY

echo "make certificate world readable only..."
chmod -v 0444 $CLIENT_SUBPATH/$CA_PUB_KEY $SERVER_SUBPATH/$SERVER_CERT $CLIENT_SUBPATH/$CLIENT_CERT $SERVER_SUBPATH/$CA_PUB_KEY

# copy ca.pem, cert.pem and key.pem to /var/lib/jenkins-slave/.docker folder on jenkins-slave
# copy ca.pem, server-cert.pem and server-key.pem to /etc/docker/ssl folder on deploy

#read -rsp $'Press any key to continue...\n' -n1 key