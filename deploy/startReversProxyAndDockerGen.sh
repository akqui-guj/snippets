#!/bin/bash

mkdir /var/lib/nginx
chmod 777 /var/lib/nginx

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# copy nginx.tmpl to /var/lib/nginx

# open port 8080
# Firewall für Port 2222 ssh öffnen
sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload

# start nginx and dockergen
docker-compose up